/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p_g7;
import Ventanas.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author richy
 */
public class Proyecto2P_G7 extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        
        VentanaLogin vl = new VentanaLogin(stage);
        VentanaPedido v3 = new VentanaPedido();
        Scene scene = new Scene(v3.getRootPedido(), 800, 800);
        stage.setTitle("Ice Cream App");
        stage.setScene(scene);
        stage.show();
    }
}
