/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p_g7;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author richy
 */
public class Menu {
    private String descripcion;
    private double precio;
    private char tipo;
    
    public Menu(String descripcion, double precio, char tipo){
        this.descripcion=descripcion;
        this.precio=precio;
        this.tipo=tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public char getTipo() {
        return tipo;
    }

    public void setTipo(char tipo) {
        this.tipo = tipo;
    }
    
    @Override
    public String toString(){
        return descripcion+" "+precio+" "+tipo;
    }
    
    public static ArrayList<Menu> obtenerMenu(){
        ArrayList<Menu> menu = new ArrayList<>();
        try(BufferedReader bf = new BufferedReader(new FileReader("src/recursos/Menu.txt"))){
            String linea=bf.readLine();
            while((linea=bf.readLine())!= null){
                String[] datos = linea.split(",");
                menu.add(new Menu(datos[0],Double.parseDouble(datos[1]),datos[2].charAt(0)));
            }
        }catch(Exception e){
            System.out.println(e.toString());
        }
        return menu;
    }    
}
