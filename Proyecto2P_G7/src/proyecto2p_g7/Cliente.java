/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p_g7;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author richy
 */
public class Cliente {
    private String usuario;
    private String contraseña;
    private String nombre;
    private String apellido;
            
    public Cliente(String user, String contra, String nom, String ap){
        this.usuario=user;
        this.contraseña=contra;
        this.nombre=nom;
        this.apellido=ap;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    
    @Override
    public String toString(){
        return this.nombre+" "+this.apellido;
    }
    
    public static ArrayList<Cliente> obtenerClientes(){
        ArrayList<Cliente> clientes = new ArrayList<>();
        try(BufferedReader bf = new BufferedReader(new FileReader("src/recursos/Usuarios.txt"))){
            String linea=bf.readLine();
            while((linea=bf.readLine())!= null){
                String[] datos = linea.split(",");
                clientes.add(new Cliente(datos[0],datos[1],datos[2],datos[3]));
            }
        }catch(Exception e){
            System.out.println(e.toString());
        }
        return clientes;
    }    
}
