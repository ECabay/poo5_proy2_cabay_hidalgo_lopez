/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2p_g7;

/**
 *
 * @author richy
 */
public class ComidaSeleccionada {
    private String descripcion;
    private int cantidad;
    private double precio;
    
    public ComidaSeleccionada(String des, int cant, double pre){
        descripcion = des;
        cantidad = cant;
        precio = pre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    
}
