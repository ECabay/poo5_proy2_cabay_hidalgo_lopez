/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;


import java.awt.Color;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import java.io.IOException;
import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Popup;
import javafx.stage.Stage;
import proyecto2p_g7.Cliente;
import proyecto2p_g7.Cliente;

/**
 *
 * @author richy
 */
public class VentanaLogin {
    private GridPane rootLogin;
    private Button ingreso;
    private Stage s;
    private Alert reLogin;
    
    public VentanaLogin(Stage s){
        this.s=s;
        rootLogin = new GridPane();
        rootLogin.setStyle("-fx-background-color: white;");
        rootLogin.setMinSize(600, 400); 
        rootLogin.setPadding(new Insets(20, 10, 50, 10)); 
        rootLogin.setVgap(30); 
        rootLogin.setHgap(30);       
      
      //Setting the Grid alignment 
        rootLogin.setAlignment(Pos.CENTER);
        //rootLogin.setGridLinesVisible(true);
        crearSeccionPrincipal();
        
    }

    public GridPane getRootLogin() {
        return rootLogin;
    }

    public void setRootLogin(GridPane rootLogin) {
        this.rootLogin = rootLogin;
    }
    
    public void crearSeccionPrincipal(){
        Text title = new Text("Heladería Ice Cube");
        Text usuario = new Text("Usuario");
        Text contraseña = new Text("Contraseña");     
        TextField nomUser = new TextField(); 
        PasswordField password = new PasswordField();
        ingreso = new Button("Ingresar");
        ingreso.setStyle("-fx-background-color: ORANGE; ");
        GridPane.setColumnSpan(title, 4);
        GridPane.setHalignment(title, HPos.CENTER);
        title.setFont(Font.font("Arial",FontWeight.BOLD,35));
        title.setFill(javafx.scene.paint.Color.ORANGE);
        //Imagenes de los helados
        Image helado1 = new Image(getClass().getResourceAsStream("/recursos/helado1.jpg"));
        Image helado2 = new Image(getClass().getResourceAsStream("/recursos/helado2.jpg"));
        ImageView h1 = new ImageView(helado1);
        ImageView h2 = new ImageView(helado2);
        h1.setFitHeight(100);
        h1.setFitWidth(150);
        h2.setFitHeight(100);
        h2.setFitWidth(150);
            
        rootLogin.add(h1, 0, 5);
        rootLogin.add(h2, 3, 5);
        rootLogin.add(title,0,0);
        rootLogin.add(usuario,1,2);
        rootLogin.add(nomUser,2,2);
        rootLogin.add(contraseña,1,3);
        rootLogin.add(password,2,3);
        rootLogin.add(ingreso,2,4);
        
        ingreso.setOnAction(t -> {
            String usuarioIngresado = nomUser.getText();
            String contraseñaIngresada = password.getText();
            boolean bandera = false;
            ArrayList<Cliente> clientes = Cliente.obtenerClientes();
            for(Cliente cliente: clientes ){
                if(usuarioIngresado.equals(cliente.getUsuario()) && contraseñaIngresada.equals(cliente.getContraseña())){
                    bandera = true;
                }
            }
            resultadoLogin(bandera);
        });
    }
    
    
    public void resultadoLogin(boolean resultado){
        if(resultado == true){
            reLogin = new Alert(Alert.AlertType.INFORMATION,"Login Exitoso",ButtonType.OK);
            reLogin.setHeaderText("Resultado del Login:");
            reLogin.setTitle("");
            VentanaPedido vp = new VentanaPedido();
            Scene sc = new Scene(vp.getRootPedido(),600,600);
            reLogin.show();
            s.setScene(sc);
        }else{
            reLogin = new Alert(Alert.AlertType.INFORMATION,"Login Fallido",ButtonType.OK);
            reLogin.setTitle("");
            reLogin.setHeaderText("Resultado del Login:");
            reLogin.show();
        }
    }
}
        
    
