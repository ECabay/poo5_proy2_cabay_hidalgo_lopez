/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import proyecto2p_g7.ComidaSeleccionada;
import proyecto2p_g7.Menu;

/**
 *
 * @author richy
 */
public class VentanaPedido {
    private VBox rootPedido;
    private HBox rootOpciones;
    private HBox rootTablaOp;
    private ComboBox cbTipo;
    private ComboBox cbOrden;
    //revisar http://acodigo.blogspot.com/2015/08/tableview-control-javafx.html
    private TableView tablaPedidos;
    private TableColumn colDescrip;
    private TableColumn colCant;
    private TableColumn colVal;
    public VentanaPedido(){
        rootPedido= new VBox();
        rootOpciones = new HBox();
        rootTablaOp = new HBox();
        rootTablaOp.setSpacing(150);
        tablaPedidos = new TableView();
        tablaPedidos.relocate(1, 1);
        colDescrip= new TableColumn("Descripcion");
        colDescrip.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));
        colCant= new TableColumn("Cantidad");
        colCant.setCellValueFactory(new PropertyValueFactory<>("Cantidad"));
        colVal= new TableColumn("Valor");
        colVal.setCellValueFactory(new PropertyValueFactory<>("Precio"));
        tablaPedidos.getColumns().addAll(colDescrip,colCant,colVal);
        tablaPedidos.setVisible(false);
        //rootPedido.setStyle("-fx-background-color: white;");
        rootPedido.setBackground(Background.EMPTY);
        rootPedido.setPadding(new Insets(30, 0, 0, 30)); 
        rootPedido.setSpacing(25);
        rootOpciones.setSpacing(61);
        crearSeccionTop();
        crearComboBoxTipo();
        
        crearSeccionMid();
        
        
        
    }

    public VBox getRootPedido() {
        return rootPedido;
    }

    public void setRootPedido(VBox rootPedido) {
        this.rootPedido = rootPedido;
    }
    
    public void crearComboBoxTipo(){
        HBox hb = new HBox();
        HBox hb1 = new HBox();
        HBox hb2 = new HBox();
        hb.setPadding(new Insets(0,0,0,10));
        hb.setSpacing(350);
        cbTipo = new ComboBox();
        Text tipo = new Text("Tipo"); 
        tipo.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        cbTipo.getItems().addAll("Crema","Granizado","Normal","Sorbete");
        hb1.getChildren().addAll(tipo,cbTipo);
        Text orden = new Text("Ordenar por");
        orden.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        cbOrden = new ComboBox();
        cbOrden.getItems().add("Precio");
        cbOrden.getItems().add("Nombre");
        hb2.getChildren().addAll(orden,cbOrden);
        
        
        cbOrden.setDisable(true);
        

        hb.getChildren().addAll(hb1,hb2);
        rootPedido.getChildren().add(hb);
        
        
    }
    
    
    
    public void crearSeccionTop(){
        Text tit1 = new Text("Realice su pedido");
        tit1.setFont(Font.font("Arial", FontWeight.BOLD, 25));
        tit1.setFill(javafx.scene.paint.Color.ORANGE);
        rootPedido.getChildren().add(tit1);
    }
    
    public void crearSeccionMid(){
        HBox hb = new HBox();
        HBox labels = new HBox();
        hb.setSpacing(460);
        labels.setSpacing(40);
        Label descripcion = new Label("Descripcion");
        Label precio = new Label("Precio");
        Label cantidad = new Label("Cantidad");
        descripcion.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        precio.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        cantidad.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        labels.getChildren().addAll(descripcion,precio,cantidad);
        Text opciones = new Text("Opciones");
        opciones.setFont(Font.font("Arial", FontWeight.BOLD,20));
        opciones.setFill(Color.ORANGE);
        Text pedido = new Text("Pedido");
        pedido.setFont(Font.font("Arial",FontWeight.BOLD,20));
        pedido.setFill(Color.ORANGE);
        
        hb.getChildren().addAll(opciones,pedido);
        
        rootPedido.getChildren().addAll(hb,labels);
        
        cbTipo.valueProperty().addListener(new ChangeListener<String>() {
        @Override 
        public void changed(ObservableValue ov, String t, String t1) {
           tablaPedidos.setVisible(true);
           cbOrden.setDisable(false);
           rootOpciones.getChildren().clear();
           VBox vb = new VBox();
           vb.setSpacing(9);
           VBox vb2 = new VBox();
           vb2.setSpacing(9);
           HBox agregados = new HBox();
           agregados.setSpacing(5);
           VBox vb3 = new VBox();
           VBox botones = new VBox();
           for(Menu menu:Menu.obtenerMenu()){
                if(t1.charAt(0) == menu.getTipo()){   
                    Label lb = new Label(menu.getDescripcion());
                    Label lb2 = new Label(String.valueOf(menu.getPrecio()));
                    TextField tf = new TextField();
                    Button agregar = new Button("Agregar");
                    
                    agregar.setOnAction(e->{
                        if(tf.getText().isEmpty() == true){
                            System.out.println("El campo de cantidad está vacío");
                        }else{
                            ComidaSeleccionada cs = new ComidaSeleccionada(menu.getDescripcion(),Integer.parseInt(tf.getText()),menu.getPrecio());
                            tablaPedidos.getItems().add(cs);
                        }
                    
                    });
                    tf.setPrefSize(40, 10);
                    vb.getChildren().add(lb);
                    vb2.getChildren().add(lb2);
                    vb3.getChildren().add(tf);
                    botones.getChildren().add(agregar);
                }
            }
           agregados.getChildren().addAll(vb3,botones);
           agregados.setAlignment(Pos.CENTER_RIGHT);
           rootOpciones.getChildren().addAll(vb,vb2,agregados);
           
        }    
        
    });
        rootTablaOp.getChildren().addAll(rootOpciones,tablaPedidos);
        rootPedido.getChildren().add(rootTablaOp);
        
    }
    
   
}
