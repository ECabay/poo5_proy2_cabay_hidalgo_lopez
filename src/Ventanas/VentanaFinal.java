
package Ventanas;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class VentanaFinal {
    private VBox rootFinal;
    
    public VentanaFinal() throws Exception{
        rootFinal= new VBox();
        rootFinal.setSpacing(80);
        rootFinal.setAlignment(Pos.CENTER);
        try{
            crearSeccionFinal();
        }catch(RuntimeException ex){
            rootFinal.getChildren().clear();
            rootFinal.getChildren().add(new Label ("Ha ocurrido un error..."));
            System.out.println(ex.getMessage());
        }
    }
    
    public VBox getRoot(){
        return rootFinal;
    }
    
    public void crearSeccionFinal() throws FileNotFoundException{
        Text ty= new Text("¡MUCHAS GRACIAS POR SU COMPRA!");
        Text info= new Text("Su pedido Nro 2836 ha sido pagado y ahora empezaremos a preparalo.\n"
                + "En aproximadamente 30 minutos llegará a su destino.\n"
                + "Gracias por preferirnos.");
        Image delivery= new Image(new FileInputStream("moto.png"),300,200,false,false);
        ImageView deliveryV= new ImageView(delivery);
        ty.setFont(Font.font("Arial",FontWeight.BOLD,35));
        ty.setFill(Color.DARKSALMON);
        info.setFill(Color.BLACK);
        info.setFont(Font.font("Arial",FontWeight.BOLD,18));
        info.setTextAlignment(TextAlignment.CENTER);
        rootFinal.getChildren().addAll(ty,info,deliveryV);
    }
    
}

