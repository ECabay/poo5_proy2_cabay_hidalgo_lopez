package Ventanas;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import Ventanas.VentanaFinal;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class VentanaMenu {
    private VBox rootMenu;
    private VBox seccionMenu;
    private VBox seccionBotones;
    private Stage s;
    
    public VentanaMenu(Stage s) throws Exception{
        this.s= s;
        rootMenu= new VBox();
        rootMenu.setSpacing(70);
        rootMenu.setPadding(new Insets(180,400,0,10));
        try{
            crearFondo();
            crearSeccionMenu();
            crearSeccionBotones();
        }catch(RuntimeException ex){
            rootMenu.getChildren().clear();
            rootMenu.getChildren().add(new Label("Ha ocurrido un error"));
            System.out.println(ex.getMessage());
        }
    }
    
    public VBox getRoot(){
        return rootMenu;
    }
    
    public void crearFondo() throws FileNotFoundException{
        Image ifondo= new Image(new FileInputStream("Fondo2.jpg"),1000,700,false,false);
        BackgroundImage fondo= new BackgroundImage(ifondo,BackgroundRepeat.REPEAT,BackgroundRepeat.NO_REPEAT,BackgroundPosition.DEFAULT,BackgroundSize.DEFAULT);
        rootMenu.setBackground(new Background(fondo));
    }
    
    public void crearSeccionMenu(){
        seccionMenu= new VBox();
        seccionMenu.setSpacing(30);
        Text welcome =new Text("Bienvenid@ Edison");
        welcome.setFont(Font.font("Arial",FontWeight.BOLD,50));
        welcome.setFill(Color.DARKSALMON);
        Text opcion= new Text("Elije la opción que prefieras");
        opcion.setFont(Font.font("Arial",FontWeight.BOLD,25));
        seccionMenu.getChildren().addAll(welcome,opcion);
        seccionMenu.setAlignment(Pos.CENTER);
        rootMenu.getChildren().add(seccionMenu);
    }
    
    public void crearSeccionBotones() throws Exception{
        seccionBotones= new VBox();
        seccionBotones.setPadding(new Insets(10,10,10,10));
        seccionBotones.setSpacing(30);
        Button local= new Button("Encuentra el local más cercano");
        Button pedido= new Button("Haz tu pedido");
        local.setFont(Font.font("Arial",FontWeight.BOLD,20));
        pedido.setFont(Font.font("Arial",FontWeight.BOLD,20));
        local.setStyle("-fx-background-color: DARKSALMON;");
        pedido.setStyle("-fx-background-color: DARKSALMON;");
        local.setPrefSize(350, 50);
        pedido.setPrefSize(350, 50);
        seccionBotones.getChildren().addAll(local,pedido);
        seccionBotones.setAlignment(Pos.CENTER);
        rootMenu.getChildren().add(seccionBotones);
        VentanaFinal vf= new VentanaFinal();
        Scene Vfinal= new Scene(vf.getRoot(),1000,700);
        pedido.setOnAction(e ->s.setScene(Vfinal));
        
    }
    
}
