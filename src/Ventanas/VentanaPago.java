
package Ventanas;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


public class VentanaPago {
    private VBox rootPago;
    private HBox seccionBotones;
    private VBox seccionEntrega;
    private VBox seccionDetalle;
    private Scene s;
    
    public VentanaPago(){
        this.s=s;
        rootPago= new VBox();
        rootPago.setSpacing(40);
        rootPago.setPadding(new Insets(50,0,0,50));
        seccionEntrega();
        seccionDetalle();
    }
    
    public VBox getRoot(){
        return rootPago;
    }
    
    public void seccionEntrega(){
        seccionEntrega= new VBox();
        //seccionEntrega.setPadding(new Insets(0,30,0,0));
        HBox cajaDir= new HBox();
        Label entrega= new Label("Dirección de entrega");
        entrega.setStyle("-fx-text-fill: DARKSALMON;");
        entrega.setFont(Font.font("Arial",FontWeight.BOLD,40));
        Label dir= new Label("Dirección:");
        TextField info= new TextField();
        cajaDir.getChildren().addAll(dir,info);
        seccionEntrega.getChildren().addAll(entrega,cajaDir);
        rootPago.getChildren().add(seccionEntrega);
        
    }
    
    public void seccionDetalle(){
        seccionDetalle= new VBox();
        
        HBox cajaPagos= new HBox();
        cajaPagos.setSpacing(50);
        cajaPagos.setPadding(new Insets(0,0,0,0));
        Label detalle= new Label("Detalle de pago");
        detalle.setStyle("-fx-text-fill: DARKSALMON;");
        detalle.setFont(Font.font("Arial",FontWeight.BOLD,40));
        CheckBox efectivo= new CheckBox("Efectivo");
        CheckBox tarjeta= new CheckBox("Tarjeta de crédito");
        cajaPagos.getChildren().addAll(efectivo,tarjeta);
        seccionDetalle.getChildren().addAll(detalle,cajaPagos);
        rootPago.getChildren().add(seccionDetalle);
        
    }
}   

