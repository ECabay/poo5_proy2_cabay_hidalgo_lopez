/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package goodburgerapp;

import Ventanas.VentanaFinal;
import javafx.application.Application;
import javafx.stage.Stage;
import Ventanas.VentanaMenu;
import Ventanas.VentanaPago;
import javafx.scene.Scene;
        
public class IceCubeApp extends Application{

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        VentanaMenu vm= new VentanaMenu(stage);
        //VentanaFinal vf= new VentanaFinal();
        VentanaPago vp= new VentanaPago();
        Scene scene= new Scene(vp.getRoot(),1000,700);
        stage.setTitle("Prueba");
        stage.setScene(scene);
        stage.show();
    }
    
}
